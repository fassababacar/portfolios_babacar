import cv from './mesAssets/cv.png'
import python_django from './mesAssets/python_django.png'
import arbre from './mesAssets/arbre.png'
import thumbtack from './mesAssets/thumbtack.png'
import react_leaflet from './mesAssets/react_leaflet.png'
import geonode_icon from './mesAssets/geonode_icon.png'

function App() {
  return (
    <div id= "portfolio" className="container mb-3 mt-5">
		<div className="white-divider"></div>
		<div className="heading">
			<h1>Portfolios</h1>
		</div>
		<div className="row">
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application présentant mon CV en Ligne</h5>
				<a href="https://fassababacar.gitlab.io/curriculum_babacar_fassa/" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={cv} alt="Mon CV"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap
				</div>
			</div>
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application Web-Gis ou Web-mapping</h5>
				<a href="https://djangoprojectspaetude.onrender.com" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={python_django} alt="Projet Web Controle données GraceTHD"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap, Django, JavaScript, LeafLet
				</div>
			</div>
				<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application Arbre Généalogique Familiale</h5>
				<a href="https://fassababacar.gitlab.io/dynastie_fassa_react_app/" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={arbre} alt="Arbre Généalogique"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap, React
				</div>
			</div>
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application CRUD & pagination</h5>
				<a href="https://django-exemples.herokuapp.com/OngleListView/" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={python_django} alt="CRUD"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap, Django, JavaScript
				</div>
			</div>
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application API Spatiale</h5>
				<a href="https://thumbtackapi.onrender.com" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={thumbtack} alt="APISP"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap, Django, JavaScript
				</div>
			</div>
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application WebMapping: React-LeafLet</h5>
				<a href="https://react-leaflet-mapping.onrender.com" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={react_leaflet} alt="RLWA"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap, React
				</div>
			</div>
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application E-Learning</h5>
				<a href="https://tedrim.onrender.com/" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={python_django} alt="RLWA"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: Html, CSS, Bootstrap, React, Django
				</div>
			</div>
			<div className="card col-md-4">
				<h5 className = "card-header text-white bg-dark">Application GeoNode sur des données GraceTHD</h5>
				<a href="https://stable.demo.geonode.org/maps/6634/embed" target="_blank" rel="noreferrer">
					<img className="img-thumbnail" src={geonode_icon} alt="RLWA"></img>
				</a>
				<div className="d-flex justify-content-start card-footer text-muted">
					Langages: GeoNode
				</div>
			</div>
		</div>
    </div>
  );
}

export default App;
